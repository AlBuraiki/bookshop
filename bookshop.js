
'use strict'

const request = require('./modules/request')
const google = require('./modules/google')
const persistence = require('./modules/persistence')
const amazon =  require('./modules/amazon')
/**
 * Searches for books
 *
 * @function search
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.search = (req, callback) => {
	let query = undefined
	try {
		query = request.getParameter(req, 'q')
	} catch(err) {
		callback(err)
	}
	google.searchByString(query, (err, data) => {
		if (err) {
			callback(err)
			return
		}
		const clean = request.replaceHostname(req, data)
		callback(null, clean)
	})
}


/**
 * Returns a book with the matching ISBN number
 *
 * @function getBook
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.getBook = (req, callback) => {
	let isbn = undefined
	try {
		isbn = request.getParameter(req, 'isbn')
	} catch(err) {
		callback(err)
	}
	google.getByISBN(isbn, (err, data) => {
		if(err) {
			callback(err)
			return
		}
		callback(null, data)
	})
}

/**
 * Adds a new user.
 *
 * @function addUser
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.addUser = (req, callback) => {
	console.log('addUser')
	let credentials
	try {
		credentials = request.getCredentials(req)
		if (request.getHeader(req, 'content-type') !== 'application/json') {
			throw new Error('content-type header must be application/json')
		}
	} catch(err) {
		callback(err)
		return
	}
	//find if username does not already exists
	persistence.validUsername(credentials, (err, data) => {
		if (err) {
			console.log('validUsername error')
			callback(err)
			return
		}
		console.log(data)
		if (data === true) {
			callback(new Error('username already exists'))
			return
		}
		console.log()
		const name = request.extractBodyKey(req, 'name')
		const user = {name: name}
		console.log(user)
		persistence.addAccount(credentials, user, (err, data) => {
			if (err) {
				callback(err)
				return
			}
			callback(null, data)
			return
		})
	})
}

/**
 * Adds a book to the shooping cart associated with a valid user.
 *
 * @function addToCart
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.addToCart = (req, callback) => {
	let credentials, isbn
	try {
		credentials = request.getCredentials(req)
		isbn = request.extractBodyKey(req, 'isbn')
	} catch(err) {
		callback(err)
		return
	}
	persistence.validCredentials(credentials, (err, data) => {
		if (err) {
			callback(err)
			return
		}
		if (data === false) {
			callback(new Error('invalid credentials'))
			return
		}
		google.getByISBN(isbn, (err, data) => {
			if (err) {
				callback(err)
				return
			}
			console.log('we have a valid account and a valid ISBN')
			persistence.addBook(credentials, data, (err, data) => {
				console.log('E')
				if (err) {
					console.log('F')
					callback(err)
					return
				}
				callback(null, data)
			})
		})
	})
}

exports.getCart = (req, callback) => {
	let credentials
	try {
		credentials = request.getCredentials(req)
	} catch(err) {
		callback(err)
		return
	}
	persistence.validCredentials(credentials, (err, data) => {
		if (err) {
			callback(err)
			return
		}
		if (data === false) {
			callback(new Error('invalid credentials'))
			return
		}
		console.log('valid credentials')
		persistence.getBooksInCart(credentials, (err, data) => {
			if (err) {
				console.log(err.message)
				callback(err)
				return
			}

			callback(null, {cart: data})
		})
	})
}

/**
 * get the price of a book from amazon.
 *
 * @function getPrice
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
 
exports.getPrice = (req, callback) => {

	let query = undefined
	try {
		query = request.getParameter(req, 'isbn')
	} catch(err) {
		callback(err)
	}
	
	console.log("call amazon")
	amazon.getPrice(query, (err, data) =>{
	
		if (err) {
			callback(err)
			return
		}
		console.log("amazon finished")
		callback(null, data)
	})
	
}

/**
 * Adds a book to the stock with quantity.
 *
 * @function addRoStock
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.addToStock = (req, callback) => {

	let credentials
	try {
		credentials = request.getCredentials(req)
		if (request.getHeader(req, 'content-type') !== 'application/json') {
			throw new Error('content-type header must be application/json')
		}
	} catch(err) {
		callback(err)
		return
	}
	persistence.validCredentials(credentials, (err, data) => {
		if (err) {
			console.log('validUsername error')
			callback(err)
			return
		}
		let isbn = undefined
		try{
			isbn = request.extractBodyKey(req, 'isbn')
		}
		catch(err) {
			callback(err)
			return
		}
		
		const quantity = request.extractBodyKey(req, 'quantity')
		if(quantity < 0)
			callback(err)
		const stockItem = {isbn: isbn, quantity: quantity}
		persistence.addStock(stockItem, (err, data) => {
			if (err) {
				callback(err)
				return
			}
			callback(null, data)
			return
		})
	})
}

/**
 * Search titles in the stock only
 *
 * @function searchStock
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.searchStock = (req, callback) => {

	let query = undefined
	try {
		query = request.getParameter(req, 'q')
	} catch(err) {
		callback(err)
	}
	persistence.getBooksInStock((err, allBooks) => {
	
		let selected = []
		let counter = 0
		const len = allBooks.length
	    //find which books name matches the search term
	    allBooks.forEach(function(entry) {
	    	
	    	console.log(entry)
	    	google.getByISBN(entry.isbn, (err, data)=> {
	    		
	    		//when google return book info check if it matches the search query
	    		if(data.title.toLowerCase().includes(query) ){
	    			selected.push(data)
	    		}
	    		counter++
	    		if(counter >= len){
	    			console.log("TOTAL selected size is:" + selected.length)
	    			callback(null, selected)
	    		}
	    			
	    	})
	    })
		
	});
}

/**
 * Authenticate a user for login.
 *
 * @function login
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.login = (req, callback) => {

	let credentials
	try {
		credentials = request.getCredentials(req)
		if (request.getHeader(req, 'content-type') !== 'application/json') {
			throw new Error('content-type header must be application/json')
		}
	} catch(err) {
		callback(err)
		return
	}
	persistence.validCredentials(credentials, (err, data) => {
		if (err) {
			console.log('validUsername error')
			callback(err)
			return
		}
		persistence.loginUser(credentials, (err, data) => {
			if (err) {
				callback(err)
				return
			}
			callback(null, data)
			return
		})
	})
}

/**
 * logout a user.
 *
 * @function logout
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.logout = (req, callback) => {

	let credentials
	try {
		credentials = request.getCredentials(req)
		if (request.getHeader(req, 'content-type') !== 'application/json') {
			throw new Error('content-type header must be application/json')
		}
	} catch(err) {
		callback(err)
		return
	}
	persistence.validCredentials(credentials, (err, data) => {
		if (err) {
			console.log('validUsername error')
			callback(err)
			return
		}
		let token = undefined
		try{
			token = request.extractBodyKey(req, 'token')
		}
		catch(err) {
			callback(err)
			return
		}
		
		persistence.logoutUser(credentials, token, (err, data) => {
			if (err) {
				callback(err)
				return
			}
			callback(null, data)
			return
		})
	})
}

/**
 * Checkout a user cart
 *
 * @function checkout
 * @param req the request object passed by the API
 * @param {Function(Error, String)} callback run on completion
 */
exports.checkout = (req, callback) => {

	let credentials
	try {
		credentials = request.getCredentials(req)
		if (request.getHeader(req, 'content-type') !== 'application/json') {
			throw new Error('content-type header must be application/json')
		}
	} catch(err) {
		callback(err)
		return
	}
	persistence.validCredentials(credentials, (err, data) => {
		if (err) {
			console.log('validUsername error')
			callback(err)
			return
		}
		persistence.getBooksInCart(credentials, (err, data) => {
			if (err) {
				callback(err)
				return
			}
			
			let counter = 0
			const len = data.length
			data.forEach(function (item){
				persistence.editStok(item.isbn, (err, bookdata) => {
					if(err) throw err
					counter++;
					if(counter >= len){
						callback(null, data)	
					}
				})
			})
		})
	})
}