'use strict'

var request = require('request');
var cheerio = require('cheerio');

/** get the price of a book from amazon
 * @function getPrice
 * @param {String} isbn the isbn of the book to findd its price
 * @param {Function(Error, String)} callback run on completion
 */

exports.getPrice = (isbn, callback) => {

    var data = []
    const url = `https://www.amazon.co.uk/dp/${isbn}`
    //const url = `https://www.amazon.co.uk/dp/1593277571/`
    request(url, function (error, response, html) {
      if (!error && response.statusCode == 200) {
        var $ = cheerio.load(html);
        $('span.a-color-price').each(function(i, element){
         
            var a = $(this).parent();
            if(a.hasClass( 'a-color-base' )){
                console.log($(this).text().trim());
                data.push($(this).text().trim().replace( /[^\d\.]*/g, ''))
            }
        });
      }
      callback(null, data)
    });
}


